package es.alterparadox.umbrasdeparadox.database;

/**
 * Created by Mary on 05/07/2017.
 */

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import es.alterparadox.umbrasdeparadox.Constants;

public class DatabaseConnection extends SQLiteOpenHelper {
    private static String DB_PATH = Constants.getDbPath();
    private static String DB_NAME = Constants.getDbName();
    private SQLiteDatabase umbrasDB;
    private String sqlCreateTable = "CREATE TABLE actividades (id TEXT UNIQUE PRIMARY KEY, name TEXT, shortDesc TEXT, " +
            "organizator TEXT, minPlaces TEXT, maxPlaces TEXT, enrolled TEXT, intDate TEXT, zone TEXT, status TEXT," +
            " lastUpdated TEXT, category TEXT, createdDB TEXT)";


    public DatabaseConnection(Context context, String name, CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreateTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS actividades");
        db.execSQL(sqlCreateTable);
    }


    public boolean checkDataBase() {
        SQLiteDatabase checkDB = null;

        try {
            String myPath = DB_PATH + DB_NAME;
            checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);
        } catch (SQLiteException e) {
            // Base de datos no creada todavia
        }

        if (checkDB != null) {
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }

    public void openDatabase() throws SQLException {
        String path = DB_PATH + DB_NAME;
        umbrasDB = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
    }

    @Override
    public synchronized void close() {
        if(umbrasDB !=null) {
            umbrasDB.close();
        }
        super.close();
    }
}
