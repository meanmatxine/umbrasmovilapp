package es.alterparadox.umbrasdeparadox.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.alterparadox.umbrasdeparadox.Actividades;
import es.alterparadox.umbrasdeparadox.Constants;

/**
 * Created by Mary on 13/07/2017.
 */

public class ActividadesDAO {

    private final Context context;

    public ActividadesDAO(Context context) {
        this.context = context;
    }


    public List<Actividades> loadProgramSelectedDay(int numDay) {
        return this.loadProgramSelectedDay(numDay, ""); //any category
    }

    public List<Actividades> loadProgramSelectedDay(int numDay, String category) {
        List<Actividades> actividadesList = new ArrayList<>();

        DatabaseConnection dbConn = new DatabaseConnection(context, Constants.getDbName(), null, Constants.getDbVersion());
        SQLiteDatabase db = dbConn.getReadableDatabase();

        Long intSelectedDay = Constants.datePositionToInt(numDay)+7200; //desde las 2 am
        Long intTomorrow = intSelectedDay + 86400 ; // D�a y hora 24 horas despu�s del dato que se ha recibido
        String sqlSelect = this.getActividadesSqlQuery(intSelectedDay,intTomorrow,category);
        Cursor actividadesCursor= db.rawQuery(sqlSelect,null);
        actividadesCursor.moveToFirst();
        if (actividadesCursor.getCount() > 0) {
            //Generamos un Array con las actividades
            for (int i = 0; i < actividadesCursor.getCount(); i++) {
                Actividades actividad = new Actividades();
                actividad.setId(actividadesCursor.getString(0));
                actividad.setName(actividadesCursor.getString(1));
                actividad.setShortDesc(actividadesCursor.getString(2));
                actividad.setOrganizator(actividadesCursor.getString(3));
                actividad.setMinPlaces(actividadesCursor.getString(4));
                actividad.setMaxPlaces(actividadesCursor.getString(5));
                actividad.setEnrolled(actividadesCursor.getString(6));
                actividad.setIntDate(actividadesCursor.getString(7));
                actividad.setZone(actividadesCursor.getString(8));
                actividad.setStatus(actividadesCursor.getString(9));
                actividad.setLastUpdated(actividadesCursor.getString(10));
                actividad.setCategory(actividadesCursor.getString(11));
               // actividad.setCreatedDB(actividadesCursor.getString(12));
                actividadesList.add(actividad);
                actividadesCursor.moveToNext();
            }
        }

        actividadesCursor.close();
        db.close();


        Collections.sort(actividadesList);
        return actividadesList;
    }

    private String getActividadesSqlQuery(Long intSelectedDay, Long intTomorrow, String category) {
        category="%" + category + "%";
        return String.format("SELECT * FROM actividades WHERE intDate >= '%s' AND intDate < '%s' AND category like '%s' AND status <> '%s' ORDER BY intDate, id",
                intSelectedDay.toString(), intTomorrow.toString(),
                category,"-1");
    }
}
