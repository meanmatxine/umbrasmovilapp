package es.alterparadox.umbrasdeparadox.database;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;
import es.alterparadox.umbrasdeparadox.Actividades;
import es.alterparadox.umbrasdeparadox.Constants;
import es.alterparadox.umbrasdeparadox.clients.ActividadesRestClient;
import es.alterparadox.umbrasdeparadox.database.DatabaseConnection;

/**
 * Created by Mary on 13/07/2017.
 */

@Deprecated
public class ActividadesPersistenceManager  extends AsyncTask<String, Void, Void> {

    private final Context context;
    private final SharedPreferences preferences;
    private final String sharedPreferencesSince;
    private ArrayList<Actividades> actividadesArray = new ArrayList<>();
    private Date dateUpdate;

    public ActividadesPersistenceManager(Context context,SharedPreferences preferences, String sharedPreferencesSince) {
        this.context = context;
        this.preferences = preferences;
        this.sharedPreferencesSince = sharedPreferencesSince;
    }


    @Override
    protected void onPreExecute() {
        // Do nothing
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected Void doInBackground(String... strings) {
        return this.loadProgram();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        this.persistProgram();
    }

    private Void loadProgram() {

        List<Header> headers = new ArrayList<Header>();
        headers.add(new BasicHeader("Accept", "application/json"));

        dateUpdate = new Date();
        ActividadesRestClient.get(context, "", headers.toArray(new Header[headers.size()]),
                null, new JsonHttpResponseHandler() {

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            JSONArray responseArray = (JSONArray) response.get("actividades");
                            actividadesArray = new ArrayList<>();

                            for (int i = 0; i < responseArray.length(); i++) {
                                Actividades act = new Actividades(responseArray.getJSONObject(i));
                                if(act.getStatus().equals("0")) {
                                    actividadesArray.add(act);
                                }
                            }

                            Collections.sort(actividadesArray);

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        super.onSuccess(statusCode, headers, responseString);
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray responseArray) {
                        super.onSuccess(statusCode, headers, responseArray);
                        try {

                            for (int i = 0; i < responseArray.length(); i++) {
                                Actividades act = new Actividades(responseArray.getJSONObject(i));
                                //if(act.getStatus().equals("0")) {
                                    actividadesArray.add(act);
                                //}
                            }

                            Collections.sort(actividadesArray);

                        }catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                    }
                });
        return null;
    }


    private Void persistProgram() {

        String since = preferences.getString(Constants.SHARED_PREFERENCES_SINCE, null);
        Long sinceLong = Long.parseLong(since);

        // Ahora guardamos los objetos en la base de datos
        DatabaseConnection dbConn = new DatabaseConnection(context, Constants.getDbName(), null, Constants.getDbVersion());

        SQLiteDatabase db = dbConn.getWritableDatabase();
        Cursor ids = db.rawQuery("SELECT id FROM actividades", null);
        List<String> listaIDActividades = new ArrayList<String>();
        ids.moveToFirst();
        for (int i = 0; i < ids.getCount(); i++) {
            listaIDActividades.add(ids.getString(0));
            ids.moveToNext();
        }

        String sql = "";

        for (Actividades actividad : this.actividadesArray) {
            String id = actividad.getId();
            String name = actividad.getName();
            String shortDesc = actividad.getShortDesc();
            String organizator = actividad.getOrganizator();
            String minPlaces = "0";
            String maxPlaces = actividad.getMaxPlaces();
            String enrolled = actividad.getEnrolled();
            String intDate = actividad.getIntDate();
            String zone = actividad.getZone();
            String status = actividad.getStatus();
            String lastUpdated = actividad.getLastUpdated();
            Long lastUpdatedLong = Long.parseLong(lastUpdated);
            String category = Constants.getCategoryFromTitle(name);

            //Si ya existe en DB, update
            try {
                if (this.existsActivity(db, id) && lastUpdatedLong > sinceLong) {

                    sql = String.format("UPDATE actividades "
                            + "SET name = '%s', shortDesc = '%s', "
                            + "organizator = '%s', minPlaces = '%s', maxPlaces = '%s', "
                            + "enrolled = '%s', intDate = '%s', zone = '%s', "
                            + "status = '%s', lastUpdated = '%s', "
                            + "category = '%s' "
                            + "WHERE id = '%s'", name, shortDesc, organizator, minPlaces, maxPlaces, enrolled, intDate, zone, status, lastUpdated, category, id);
                } else { //si no existe en DB, insert
                    String createdDB = ((new Date().getTime()) / 1000) + "";
                    sql = String.format("INSERT INTO actividades "
                            + "(id, name, shortDesc, organizator, minPlaces, maxPlaces, enrolled, intDate, zone, status, lastUpdated,category,createdDB) "
                            + "VALUES "
                            + "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s','%s')", id, name, shortDesc, organizator, minPlaces, maxPlaces, enrolled, intDate, zone, status, lastUpdated, category, createdDB);
                    // Este id se a�ade por si hay dos actualizaciones sobre la misma actividad
                    listaIDActividades.add(id);
                }
                db.execSQL(sql);
            } catch (Exception e) {
                e.getMessage();
            }
        }

        ids.close();
        dbConn.close();

        this.updatePreferencesSince();

        return null;
    }

    private boolean existsActivity(SQLiteDatabase db,String id) {
        String sqlSelect = String.format("SELECT * FROM actividades WHERE id = '%s'",id);
        Cursor cursor= db.rawQuery(sqlSelect,null);
        boolean exists = cursor.getCount() > 0;
        cursor.close();
        return exists;
    }

    private void updatePreferencesSince() {
        // Actualizamos la fecha de actualizacion. La proxima vez que se inicie la aplicacion pedira
        //las actividades actualizadas desde antes de hacer la peticion
        Long updated = dateUpdate.getTime() / 1000;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.SHARED_PREFERENCES_SINCE, updated.toString());
        editor.commit();
    }

}
