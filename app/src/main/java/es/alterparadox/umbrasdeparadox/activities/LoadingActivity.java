package es.alterparadox.umbrasdeparadox.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import es.alterparadox.umbrasdeparadox.R;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
    }
}
