package es.alterparadox.umbrasdeparadox.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import es.alterparadox.umbrasdeparadox.Constants;
import es.alterparadox.umbrasdeparadox.R;

public class HelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);


        ImageView iconLudoteca = (ImageView) findViewById(R.id.icon_category_LUDOTECA);
        iconLudoteca.setColorFilter(Constants.getColorForCategory(Constants.CategoryEnum.JUEGO_MESA.toString()));

        ImageView iconREV = (ImageView) findViewById(R.id.icon_category_REV);
        iconREV.setColorFilter(Constants.getColorForCategory(Constants.CategoryEnum.REV.toString()));


        ImageView iconROL = (ImageView) findViewById(R.id.icon_category_ROL);
        iconROL.setColorFilter(Constants.getColorForCategory(Constants.CategoryEnum.ROL.toString()));

//        ImageView iconDEMO = (ImageView) findViewById(R.id.icon_category_DEMO);
//        iconDEMO.setColorFilter(Constants.getColorForCategory(Constants.CategoryEnum.DEMO.toString()));

        ImageView iconTORNEO = (ImageView) findViewById(R.id.icon_category_TORNEO);
        iconTORNEO.setColorFilter(Constants.getColorForCategory(Constants.CategoryEnum.TORNEO.toString()));


       ImageView iconOTROS = (ImageView) findViewById(R.id.icon_category_WARGAME);
        iconOTROS.setColorFilter(Constants.getColorForCategory(Constants.CategoryEnum.WARGAME.toString()));

    }
}
