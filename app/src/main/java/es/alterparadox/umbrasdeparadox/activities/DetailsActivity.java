package es.alterparadox.umbrasdeparadox.activities;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import es.alterparadox.umbrasdeparadox.Actividades;
import es.alterparadox.umbrasdeparadox.R;

public class DetailsActivity extends AppCompatActivity {

    private Actividades actividades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.actividades =(Actividades) getIntent().getSerializableExtra("Actividades");

        setContentView(R.layout.activity_details);

        TextView title = (TextView) findViewById(R.id.details_title);
        title.setText(this.getActividades().getName());

        TextView time = (TextView) findViewById(R.id.details_date);
        Date date = new Date(Long.parseLong(actividades.getIntDate())*1000);
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        df.setTimeZone(TimeZone.getTimeZone("GTM-2"));
        time.setText(df.format(date));

        TextView organizator = (TextView) findViewById(R.id.details_organizator_value);
        if(!this.getActividades().getOrganizator().isEmpty()) {
            organizator.setText(this.getActividades().getOrganizator());
        } else {
            organizator.setText("-");
        }

        TextView zona = (TextView) findViewById(R.id.details_zona_value);
        if(!this.getActividades().getZone().isEmpty()) {
            zona.setText(this.getActividades().getZone());
        } else {
            zona.setText("-");
        }

        TextView plazas = (TextView) findViewById(R.id.details_plazas_value);
        if(this.getActividades().getMaxPlaces().equals("0")) {
            plazas.setText("Sin límite");
        } else {
            plazas.setText(this.getActividades().getMaxPlaces());
        }
//        if(this.getActividades().getMinPlaces().equals(0) && this.getActividades().getMaxPlaces().equals(0)) {
//            plazas.setText("Sin límite");
//        } else {
//            plazas.setText(this.getActividades().getMinPlaces() + "-" + this.getActividades().getMaxPlaces());
//        }

        TextView apuntados = (TextView) findViewById(R.id.details_apuntados_value);
        apuntados.setText(this.getActividades().getEnrolled());

        TextView description = (TextView) findViewById(R.id.details_description);
        description.setText(this.getActividades().getShortDesc());



    }

    public Actividades getActividades() {
        return actividades;
    }
}
