package es.alterparadox.umbrasdeparadox.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.message.BasicHeader;
import es.alterparadox.umbrasdeparadox.Actividades;
import es.alterparadox.umbrasdeparadox.Constants;
import es.alterparadox.umbrasdeparadox.R;
import es.alterparadox.umbrasdeparadox.adapters.ActividadesAdapter;
import es.alterparadox.umbrasdeparadox.database.ActividadesPersistenceManager;
import es.alterparadox.umbrasdeparadox.clients.ActividadesRestClient;
import es.alterparadox.umbrasdeparadox.database.ActividadesDAO;
import es.alterparadox.umbrasdeparadox.helper.MyReceiver;

public class MainActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);


        createAlarm();
        // Si no tiene datos lanzar la actividad de loading

      // new ActividadesPersistenceManager(this,this.getSharedPreferences(),this.getSharedPreferencesSince()).execute();

    }

    private SharedPreferences getSharedPreferences() {
        return getSharedPreferences(Constants.SHARED_PREFERENCES_TITLE, Context.MODE_PRIVATE);
    }


    private String getSharedPreferencesSince() {
        String lastUpdateMain =  this.getSharedPreferences().getString(Constants.SHARED_PREFERENCES_SINCE, null);
        if (lastUpdateMain == null) {
            SharedPreferences.Editor editor = this.getSharedPreferences().edit();
            editor.putString(Constants.SHARED_PREFERENCES_SINCE, "100");
            editor.commit();
        }
        return this.getSharedPreferences().getString(Constants.SHARED_PREFERENCES_SINCE, null);
    }

    private void updateSharedPreferencesSince() {
        SharedPreferences.Editor editor = this.getSharedPreferences().edit();
        String timeInSecondsStr = (System.currentTimeMillis()/1000) + "";
        editor.putString(Constants.SHARED_PREFERENCES_SINCE, timeInSecondsStr);
        editor.commit();

    }

    private void createAlarm() {
        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(this, MyReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(this, Constants.ALARM_REQUEST_CODE, intent,  PendingIntent.FLAG_CANCEL_CURRENT);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),AlarmManager.INTERVAL_HOUR, pIntent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_normative) {
            Intent intent = new Intent(this, NormativeActivity.class);
            this.startActivity(intent);
        } else if (id == R.id.action_map) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constants.UMBRAS_MAP_URL));
            startActivity(browserIntent);
        } else if (id == R.id.action_help) {
            Intent helpIntent = new Intent(this, HelpActivity.class);
            this.startActivity(helpIntent);
        //} else if (id == R.id.action_update) {
        //   new ActividadesPersistenceManager(this,this.getSharedPreferences(),this.getSharedPreferencesSince()).execute();
        //    Toast.makeText(this, R.string.action_update_text,
        //           Toast.LENGTH_LONG).show();
        }

        return true;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            ActividadesDAO actividadesDAO = new ActividadesDAO(this.getContext());
            List<Actividades> actividadesListDAO= actividadesDAO.loadProgramSelectedDay(getArguments().getInt(ARG_SECTION_NUMBER));

            ArrayList<Actividades> actividadesArray = new ArrayList<Actividades>();
            actividadesArray.addAll(actividadesListDAO);

            ActividadesAdapter actividadesAdapter = new ActividadesAdapter(this.getContext(), actividadesArray);
            ListView listViewActividades = (ListView)rootView.findViewById(R.id.actividades_list);

            listViewActividades.setAdapter(actividadesAdapter);
            listViewActividades.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                    Actividades act = (Actividades) adapterView.getItemAtPosition(position);
                    Intent detailsActivity = new Intent(getActivity(),DetailsActivity.class);
                    detailsActivity.getExtras();
                    detailsActivity.putExtra("Actividades",act);
                    startActivity(detailsActivity);
                }
            });

         //   TextView textView = (TextView) rootView.findViewById(R.id.section_label);
          //  textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 4 total pages, one per day
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Jueves 16";
                case 1:
                    return "Viernes 17";
                case 2:
                    return "Sábado 18";
                case 3:
                    return "Domingo 19";
            }
            return null;
        }
    }



}
