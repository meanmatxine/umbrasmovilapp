package es.alterparadox.umbrasdeparadox.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import es.alterparadox.umbrasdeparadox.database.ActividadesAlarmedPersistenceManager;

/**
 * Created by Mary on 25/02/2018.
 */

public class MyReceiver extends BroadcastReceiver {


    Context context;
    Intent intent;
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
        this.intent = intent;

        new ActividadesAlarmedPersistenceManager(context).execute();
    }

}
