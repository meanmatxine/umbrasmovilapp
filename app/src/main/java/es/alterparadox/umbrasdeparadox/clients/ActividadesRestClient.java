package es.alterparadox.umbrasdeparadox.clients;

/**
 * Created by Mary on 06/07/2017.
 */


import android.content.Context;
import android.os.Looper;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import cz.msebera.android.httpclient.Header;
import es.alterparadox.umbrasdeparadox.Constants;

public class ActividadesRestClient {
        private static final String BASE_URL = "http://alterparadox.es/example.json";
    // private static final String BASE_URL = "http://www.singermorning.com";

        private static AsyncHttpClient asyncHttpClient = new AsyncHttpClient();
        public static AsyncHttpClient syncHttpClient= new SyncHttpClient();

        public static void get(Context context, String url, Header[] headers, RequestParams params,
                               AsyncHttpResponseHandler responseHandler) {
            ActividadesRestClient.getClient().get(context, ActividadesRestClient.getAbsoluteUrl(url), headers, params, responseHandler);
        }

        private static String getAbsoluteUrl(String relativeUrl) {
            return BASE_URL + relativeUrl;
        }


    /**
     * @return an async client when calling from the main thread, otherwise a sync client.
     */
    private static AsyncHttpClient getClient()
    {
        // Return the synchronous HTTP client when the thread is not prepared
        if (Looper.myLooper() == null) {
            return syncHttpClient;
        }
        return asyncHttpClient;
    }
}
