package es.alterparadox.umbrasdeparadox;

import android.graphics.Color;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Mary on 04/07/2017.
 */

public class Constants {

    // Cada año sólo habría que actualizar estos datos
    private static String dayOne = "16-08-2018";
    private static String dayTwo = "17-08-2018";
    private static String dayThree = "18-08-2018";
    private static String dayFour = "19-08-2018";

    // Mapa 2017
    public static final String UMBRAS_MAP_URL = "https://www.google.com/maps/d/embed?mid=1U3k6OEDr3GtxQWF7mJsLRkO6BehKVEJK";


    private static List<String> days = new ArrayList<>();

    public static final String SHARED_PREFERENCES_SINCE = "lastUpdateMain";
    public static final String SHARED_PREFERENCES_TITLE = "preferenciasUmbras";

    private static String dbPath = "/data/data/es.alterparadox.umbrasdeparadox/databases/";
    private static String dbName = "umbras";
    private static Integer dbVersion = 1;

    private static String urlWebService = "http://www.singermorning.com/alterWebService/";
    private static String methodAsk = "servicio.php";
    private static String methodGetJSON = "mostrarActividades.php";

    public static final int ALARM_REQUEST_CODE = 1;

    public enum CategoryEnum {
        ROL,REV,JUEGO_MESA,DEMO,TORNEO,INFANTIL,OTROS, WARGAME
    }


    // Métodos para obtener estos datos - NO TOCAR -
    public static SimpleDateFormat getDateFormat() {
        return new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    }

    public static List<String> getDays() {
        days.add(dayOne);
        days.add(dayTwo);
        days.add(dayThree);
        days.add(dayFour);
        return days;
    }

    public static Long datePositionToInt(int datePosition){
        return Constants.dateToInt(Constants.getDays().get(datePosition-1));
    }

    public static Long dateToInt(String date) {
        try {
            return Constants.dateStrToDate(date).getTime() / 1000;
        } catch ( Exception e) {
            Date now = new Date();
            return (now.getTime()/1000);
        }
    }


    public static Date dateStrToDate(String date) {
        try {
            return Constants.getDateFormat().parse(date);

        } catch ( Exception e) {
            return (new Date());
        }
    }

    public static String getDbName() {
        return dbName;
    }

    public static Integer getDbVersion() {
        return dbVersion;
    }

    public static String getDbPath() {
        return dbPath;
    }

    public static String getCompleteURLAsk() {
        return urlWebService.concat(methodAsk);
    }

    public static String getCompleteURLGetJSON() {
        return urlWebService.concat(methodGetJSON);
    }

    public static String getCategoryFromTitle(String title) {
        title = title.toUpperCase();
        if(title.contains("REV")) {
            return CategoryEnum.REV.toString();
        } else if (title.contains("ROL")) {
            return CategoryEnum.ROL.toString();
        } else if (title.contains("TORNEO")){
            return CategoryEnum.TORNEO.toString();
        } else if (title.contains("WAR")) {
            return  CategoryEnum.WARGAME.toString();
        } else if (title.contains("DEMO")) {
            return CategoryEnum.DEMO.toString();
        } else if (title.contains("LUDOTECA")) {
            return CategoryEnum.JUEGO_MESA.toString();
        } else {
            return CategoryEnum.OTROS.toString();
        }
    }

    public static int getColorForCategory(String category) {
        if(category != null ) {
            category = category.toUpperCase();
            if(category.contains(CategoryEnum.REV.toString())) {
                return Color.rgb(42,118,230);
            } else if (category.contains(CategoryEnum.ROL.toString() )) {
                return Color.rgb(0,153,0);
            } else if (category.contains(CategoryEnum.TORNEO.toString())) {
                return Color.rgb(138,65,216);
            } else if (category.contains(CategoryEnum.WARGAME.toString())) {
                return Color.rgb(192,192,192);
            } else if (category.contains(CategoryEnum.DEMO.toString())) {
                return Color.rgb(66,66,66);
            } else if (category.contains(CategoryEnum.JUEGO_MESA.toString())) {
                return Color.rgb(66,66,66);
            } else if (category.contains(CategoryEnum.OTROS.toString())) {
                return Color.rgb(66,66,66);
            } else {
                return Color.rgb(66,66,66);
            }
        } else {
            return Color.rgb(66,66,66);
        }
    }
}
