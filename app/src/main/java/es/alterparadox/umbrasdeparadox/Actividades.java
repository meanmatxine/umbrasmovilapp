package es.alterparadox.umbrasdeparadox;

/**
 * Created by Mary on 05/07/2017.
 */

import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Date;

public class Actividades implements Comparable<Actividades>, Serializable{


    private String id;
    private String name;
    private String shortDesc;
    private String organizator;
    private String minPlaces;
    private String maxPlaces;
    private String enrolled;
    private String intDate;
    private String zone;
    private String status;
    private String lastUpdated;
    private String category;
    private String createdDB; //unix time activity created in db

    public Actividades(){
        super();
    }

    public Actividades(JSONObject object) {
        try {
            this.id = object.getString("actividadID");
            this.name = object.getString("actividadNombre");
            this.shortDesc = object.getString("actividadShortDesc");
            this.organizator = object.getString("actividadOrganizador");
            this.minPlaces = object.getString("actividadPlazasMin");
            this.maxPlaces = object.getString("actividadPlazasMax");
            this.enrolled = object.getString("actividadInscritos");
            this.intDate = object.getString("actividadFecha");
            this.zone = object.getString("actividadLugar");
            this.status = object.getString("actividadCancelada");
            this.lastUpdated = object.getString("actividadLastUpdate");
            this.createdDB = "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return id;
    }
    public String getName() {
        return name;
    }
    public String getShortDesc() {
        return shortDesc;
    }
    public String getOrganizator() {
        return organizator;
    }
    public String getMinPlaces() {
        return minPlaces;
    }
    public String getMaxPlaces() {
        return maxPlaces;
    }
    public String getEnrolled() {
        return enrolled;
    }
    public String getIntDate() {
        return intDate;
    }
    public String getZone() {
        return zone;
    }
    public String getStatus() {
        return status;
    }
    public String getLastUpdated() {
        return lastUpdated;
    }
    public String getCategory() {return category;    }
    public void setId(String id) {
        this.id = id;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setShortDesc(String shortDesdc) {
        this.shortDesc = shortDesdc;
    }
    public void setOrganizator(String organizator) {
        this.organizator = organizator;
    }
    public void setMinPlaces(String minPlaces) {
        this.minPlaces = minPlaces;
    }
    public void setMaxPlaces(String maxPlaces) {
        this.maxPlaces = maxPlaces;
    }
    public void setEnrolled(String enrolled) {
        this.enrolled = enrolled;
    }
    public void setIntDate(String intDate) {
        this.intDate = intDate;
    }
    public void setZone(String zone) {
        this.zone = zone;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public void setLastUpdated(String lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    public void setCategory(String category) {this.category = category;    }

    public String getCreatedDB() {
        return createdDB;
    }

    public void setCreatedDB(String createdDB) {
        this.createdDB = createdDB;
    }

    public boolean isNew() {
        try{
            return (this.createdDB != null
                    && inLast8Hours(new java.util.Date((long) Long.parseLong(this.createdDB)*1000))
                    && this.lastUpdated != null
                    && inLast8Hours(new java.util.Date((long) Long.parseLong(this.lastUpdated)*1000))
            );
        }catch(Exception e) {
            return false;
        }
    }

    static final long DAY = 24 * 60 * 60 * 1000;
    public boolean inLastDay(Date aDate) {
        return aDate.getTime() > System.currentTimeMillis() - DAY;
    }

    static final long EIGHT_HOURS = 8 * 60 * 60 * 1000;
    public boolean inLast8Hours(Date aDate) {
        return aDate.getTime() > System.currentTimeMillis() - EIGHT_HOURS;
    }

    @Override
    public int hashCode() {
        return(this.getId() != null)?new Integer(this.getId()):super.hashCode();
    }

    @Override
    public int compareTo(@NonNull Actividades actividades) {
        if(this.intDate != null && actividades.getIntDate() != null ) {
            return this.getIntDate().compareTo(actividades.getIntDate());
        }
        return this.hashCode() - actividades.hashCode();
    }
}
