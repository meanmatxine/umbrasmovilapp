package es.alterparadox.umbrasdeparadox.adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.media.Image;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.alterparadox.umbrasdeparadox.Constants;
import es.alterparadox.umbrasdeparadox.R;

import es.alterparadox.umbrasdeparadox.Actividades;

/**
 * Created by Mary on 06/07/2017.
 */

public class ActividadesAdapter extends ArrayAdapter<Actividades> {

    private static class ViewHolder {
        TextView date;
        ImageView iconCategory;
        ImageView iconLock;
        ImageView iconCanceled;
        TextView title;
        ImageView isNew;
    }

    public ActividadesAdapter(Context context, ArrayList<Actividades> actividadesArray) {
        super(context, R.layout.item_actividades, actividadesArray);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Actividades actividades = getItem(position);
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();

            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_actividades, parent, false);

            viewHolder.title = (TextView) convertView.findViewById(R.id.value_actividad_title);
            viewHolder.iconCategory = (ImageView) convertView.findViewById(R.id.value_actividad_icon_category);
            viewHolder.iconLock = (ImageView) convertView.findViewById(R.id.value_actividad_lock);
            viewHolder.iconCanceled = (ImageView) convertView.findViewById(R.id.value_actividad_canceled);
            viewHolder.date = (TextView) convertView.findViewById(R.id.value_actividad_date);
            viewHolder.isNew = (ImageView) convertView.findViewById(R.id.value_actividad_icon_new);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }



        viewHolder.title.setText(this.get_ActividadTitulo(actividades));
        viewHolder.iconCategory.setColorFilter(Constants.getColorForCategory(actividades.getCategory()));


        if(this.is_ActividadCancelada(actividades)) {
            viewHolder.iconCategory.setVisibility(View.INVISIBLE);
            viewHolder.iconLock.setVisibility(View.INVISIBLE);
            viewHolder.iconCanceled.setVisibility(View.VISIBLE);
        }else if(this.is_ActividadCompleta(actividades)) {
            viewHolder.iconCategory.setVisibility(View.INVISIBLE);
            viewHolder.iconLock.setVisibility(View.VISIBLE);
            viewHolder.iconCanceled.setVisibility(View.INVISIBLE);
        } else {
            viewHolder.iconCategory.setVisibility(View.VISIBLE);
            viewHolder.iconLock.setVisibility(View.INVISIBLE);
            viewHolder.iconCanceled.setVisibility(View.INVISIBLE);
        }


        Date date = new Date(Long.parseLong(actividades.getIntDate())*1000);
        DateFormat df = new SimpleDateFormat("HH:mm");
        df.setTimeZone(TimeZone.getTimeZone("GTM-2"));
        String reportDate = df.format(date);
        viewHolder.date.setText(reportDate);
        if(actividades.isNew()) {
            viewHolder.isNew.setColorFilter(Color.rgb(204,102,0));
        } else {
            viewHolder.isNew.setVisibility(View.INVISIBLE);
        }


        return convertView;
    }


    private boolean is_ActividadCompleta(Actividades act) {
        return (act.getEnrolled() != null && act.getMaxPlaces() != null
                && !act.getEnrolled().isEmpty() && !act.getMaxPlaces().isEmpty()
                && act.getEnrolled().trim().equalsIgnoreCase(act.getMaxPlaces().trim()));
    }

    private boolean is_ActividadCancelada(Actividades act)  {
        return(!act.getStatus().equalsIgnoreCase("0"));
    }

    private String get_ActividadTitulo(Actividades act) {
        if(this.is_ActividadCancelada(act)) {
            return act.getName() +  " (CANCELADA)";
        } else {
            return act.getName() + " (" + act.getEnrolled() + "/" + act.getMaxPlaces() + ")";
        }
    }
}
